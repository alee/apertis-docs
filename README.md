# Documentation for the Apertis developer portal

This is the source for the documentation of the Apertis Developer Portal, hosted at <https://appdev.apertis.org/>. The website sources can be found at <https://git.internal.apertis.org/cgit/users/meh/backworth.git/> , instructions on how to run it can be found in this project's README.

This project contains a specialized theme and a hotdoc extension that should be used when building the documentation for the website, however using these is not required for submitting contributions, the process for doing this is explained hereafter.

> If building the documentation for usage in backworth, skip the next sections and go directly to the `Building the documentation for usage on appdev.apertis.org` section.

## Building the documentation standalone on Apertis

This project [can be built as a Debian-format
package](https://www.debian.org/doc/manuals/maint-guide/build.en.html)
on Apertis systems. When the resulting `apertis-docs` package is installed,
the HTML documentation is installed in
[/usr/share/doc/apertis-docs/html](file:///usr/share/doc/apertis-docs/html/)
and can be viewed with this command:

```
xdg-open file:///usr/share/doc/apertis-docs/html/index.html
```

# Build the documentation locally in a docker container

To avoid installing [hotdoc](https://github.com/hotdoc/hotdoc) and [pandoc](https://pandoc.org/)
by hand, the docker image used on the server side can be used to build locally as well:

```
RELEASE_VERSION="v2019"
docker run -it --rm \
    -v $(pwd):/documentation \
    -w /documentation \
    -u $(id -u) \
    --security-opt label=disable \
    docker-registry.apertis.org/apertis/apertis-${RELEASE_VERSION}-documentation-builder \
    make
```
