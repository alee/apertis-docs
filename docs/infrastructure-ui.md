---
short-description: "Generic system utilities"

authors:
    - name: Robert Bosch Car Multimedia GmbH
      years: [2015, 2016]

license: CC-BY-SAv4.0
...

# Infrastructure UI

Some apps need to be available as part of the system even if there are
no apps. Below is the list of such apps:

  - launcher – app laucher. Every app installed in the system will be
    listed here.

  - settings – Collection of system and app settings (if any)

  - statusbar – Status information shown at the top oft he screen
