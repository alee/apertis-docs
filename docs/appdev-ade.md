---
short-description: "How to build and debug your application with the ade tool"

authors:
    - name: Justin Kim
      email: justin.kim@collabora.com
      years: [2017]

license: CC-BY-SAv4.0
...

# Apertis Development Environment

Apertis Development Environment (ADE) is a tool that helps developers to
manage sysroots, cross-compile applications, deploy them to target and
then debug them.

On the Apertis SDK, the ADE tool is provided by the `apertis-dev-tools` package.

```
$ sudo apt update
$ sudo apt install apertis-dev-tools
```

Once `apertis-dev-tools` is installed, the `ade` command line tool is available for
building and debugging applications. The command also provides general manual
pages so you can get command information by using `man ade` on the SDK.

## Sample applications for ADE

Since the 17.06 release, the Apertis SDK provides the code for the sample applications checked out under the `user` home folder.
Each application aims to demonstrate how to create a bundle addressing a minimal use case.

* [helloworld-app](https://gitlab.apertis.org/sample-applications/helloworld-app) - A basic sample application which can be used as the basic skeleton for more complex application
* [helloworld-simple-agent](https://gitlab.apertis.org/sample-applications/helloworld-simple-agent) - An agent-only application which does not provide any graphical program
* [helloworld-agentapp](https://gitlab.apertis.org/sample-applications/helloworld-agentapp) - An agent application which interacts with a graphical program
* [helloworld-https-client](https://gitlab.apertis.org/sample-applications/helloworld-https-client) - A basic HTTP(S) client application
* [helloworld-persistapp](https://gitlab.apertis.org/sample-applications/helloworld-persistapp) - An application to deal with persistent data 
* [helloworld-prefsapp](https://gitlab.apertis.org/sample-applications/helloworld-prefsapp) - An application to deal with preferences
* [notification-example](https://gitlab.apertis.org/sample-applications/notification-example) - An example for using Notifications API
* [hard-keys-example](https://gitlab.apertis.org/sample-applications/hard-keys-example) - An example for using hardware keys
* [cpp-library-example](https://gitlab.apertis.org/sample-applications/cpp-library-example) - An example for using external library with C++
* [helloworld-webapp](https://gitlab.apertis.org/sample-applications/helloworld-webapp) - A basic Web Runtime application

To demonstrate how to use `ade`, [helloworld-app](https://gitlab.apertis.org/sample-applications/helloworld-app)
is going to be used through this document.

## Building and running an application bundle with ade

To install and test on the SDK, `ade` provides the `--native` option.

```
$ ade configure --native
$ ade build --native
$ ade export
$ ade install --native
$ ade run --native
```

If you want to build the application in debug mode, you should use `ade configure --native --debug`.
When the build is done, `ade export` will create a bundle file. For the `helloworld-app` example,
the command will generate the `org.apertis.HelloWorldApp-0.1.0.bundle` file under the current path.

The final step for installation is to run `ade install --native`. Then, if it is done successfully,
you can run the application by executing `ade run --native`.


# Debugging on a target device with ADE

## Installation of a sysroot

To run/debug an application bundle on a target device, the first step is to download a sysroot.
You can manage sysroots on the SDK with the `ade sysroot` command. See `man ade-sysroot` for more
details about sysroot-related options.

`ade sysroot list` is used to check which sysroots are already installed
on the SDK.  If there's no installed sysroot image, the result would be like the following
message.

```
$ ade sysroot list
No sysroot installed in directory /opt/sysroot/.
```

`ade sysroot latest` provides the version and downloadable URL of the sysroot image.

```
$ ade sysroot latest
* No distribution specified, defaulting to host distribution
* No release version specified, defaulting to host release version
* No architecture specified, defaulting to 'armhf'
* Checking latest version available for apertis - 17.06 (armhf)
* Retrieved latest version: apertis 17.06 - 20170530.0 (armhf)
* Download URL: https://images.apertis.org/sysroot/17.06/sysroot-apertis-17.06-armhf-development_20170530.0.tar.gz
```

The `ade sysroot install` command will do everything needed to install the latest sysroot version on your SDK,
from downloading to installing without any manual intervention.

```
$ ade sysroot install
* No distribution specified, defaulting to host distribution
* No release version specified, defaulting to host release version
* No architecture specified, defaulting to 'armhf'
* Installing version apertis 17.06 - 20170530.0 (armhf)
sysroot.tar.gz |==================================================| 100%
```

When the installation command has completed its job, the installed status can be checked with `ade sysroot installed`.

```
$ ade sysroot installed
* No distribution specified, defaulting to host distribution
* No release version specified, defaulting to host release version
* No architecture specified, defaulting to 'armhf'
* Retrieved current version: apertis 17.06 - 20170530.0 (armhf)
```

## Setting up for a target device

For debugging on a target device, the target should accept remote connections via SSH and
allow a specific port for remote gdb connection.


### Preparing SSH key-pair on the SDK

SSH key is required to allow connecting from SDK to the target without requesting password.
The following command will show how to generate SSH key-pair and copy the generated public
key to the target. In the example, it assumes that the address of the target device is
`192.168.0.100`.

```
$ export TARGET_HOST=192.168.0.100
$ ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/home/user/.ssh/id_rsa):
Created directory '/home/user/.ssh'.
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/user/.ssh/id_rsa.
Your public key has been saved in /home/user/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:Yd3+gA4ZpWUczdlFKFlaNsyQfET09+UJF5LZYNAaFrI user@apertis
The key's randomart image is:
+---[RSA 2048]----+
|         o=B=^&=o|
|         *++XBB+.|
|        =Eo =+ .+|
|       . + +  o.=|
|        S . o  .o|
|         o   o   |
|          .   .  |
|                 |
|                 |
+----[SHA256]-----+
$ ssh-copy-id user@$TARGET_HOST
```

### Allowing GDB Connection on a target device

Since the network firewall is enabled on the target for security reasons, the connection to the remote GDB port
(`1234` by default) must be explicitly enabled.

```
$ ssh user@$TARGET_HOST "sudo sed -i '25 a -A INPUT -p tcp -m state --state NEW -m tcp --dport 1234 -j ACCEPT' /etc/sysconfig/iptables"
$ ssh user@$TARGET_HOST "sudo systemctl restart iptables"
```

### Using .gdbinit

For debugging, we are going to use GDB remote connection so the target application will be started as soon as 
executing `ade` debug command on the SDK. During GDB startup, the minimal information such as the location of
sysroot, and debugging symbols, should be provided by `.gdbinit`.

```
$ cat > ~/.gdbinit <<EOF 
set sysroot /opt/sysroot/apertis/17.06/armhf
set debug-file-directory /opt/sysroot/apertis/17.06/armhf/usr/lib/debug
EOF

```

### Adding debug symbols for packages needed by the application

> Note: this section will become obsolete and will be dropped once https://phabricator.apertis.org/T3819 is fixed.

To get proper backtraces under GDB you need debug symbols for all the libaries in the stack used by your application.
The current sysroots do not ship the debug symbols for every installed package and a manual step is needed to fetch them.

From the SDK, the following commands will install the debug symbol packages (`libglib2.0-0-dbg`, `libclutter-1.0-dbg`, `libgtk-3-0-dbg`)
on the installed sysroot image.

```
$ bwrap --bind /opt/sysroot/apertis/17.06/armhf/ / \
        --proc /proc \
        --dev-bind /dev /dev \
        --setenv PATH /sbin:/usr/sbin:/usr/local/sbin:$PATH \
        fakeroot apt update

$ bwrap --bind /opt/sysroot/apertis/17.06/armhf/ / \
        --proc /proc \
        --dev-bind /dev /dev \
        --setenv PATH /sbin:/usr/sbin:/usr/local/sbin:$PATH \
        fakeroot apt install libglib2.0-0-dbg libclutter-1.0-dbg libgtk-3-0-dbg
```

## Debugging on a target board

Debugging an application on a target device is similar to running on the SDK.
For the following example steps, we assume that the desired sysroot has been already installed
on the SDK, that you are using a SSH key-pair to login to your target device, and that the address of
the device is mapped to `TARGET_HOST` environment variable.

To build an application bundle for your target device in debug mode, the following `ade` commands will
be used.

```
$ ade configure --debug --device user@$TARGET_HOST
$ ade build --debug --device user@$TARGET_HOST
$ ade install --device user@$TARGET_HOST
```

For debugging, `ade` provides `debug` command for a target. As we mentioned above, it requires to allow
connecting to GDB remote port (default:1234). Then, the command will bring you to GDB remote prompt.

```
$ ade debug --device user@$TARGET_HOST
```
