---
short-description: "How to build your first application"

authors:
    - name: Robert Bosch Car Multimedia GmbH
      years: [2015, 2016, 2017]
    - name: Micah Fedke
      email: micah.fedke@collabora.co.uk
      years: [2016]

license: CC-BY-SAv4.0
...

# Hello World

The SDK comes with the Eclipse IDE installed, which the application developers can use to import, build and test any sample application. The imported sample application acts as a reference and further additions and modifications can be made to it.

Here is a diagram outlining the project configuration process:

![](media/image3.png)

These are steps to be followed to import a sample application into the workspace:

* Open the Eclipse IDE by double clicking on this icon on the desktop of your SDK:

    ![](media/image4.png)

* In Eclipse IDE select **Menu**→**File**→**Import**. You will see a dialog like the one below:

    ![](media/image5.png)

* Select **General**→**Existing Projects into Workspace**.  Click **Next**. Select one of the subfolders of the `/home/user/sample-applications` directory. See the dialog below for details.

    ![](media/image6.png)

* Click on **Finish**, the sample application will be imported and configuration will start automatically as shown below (check the debugs in console).

    ![](media/image7.png)

* To build the project, right click on the project and select **Build Project**.

    ![](media/image8.png)

* The project should build without any errors:

    ![](media/image9.png)

### Install via Eclipse

* After successful build of the project, right click on the project and select **Install (Native)** option. The selected project will be installed on the simulator.

    ![](media/image10.png)

### Uninstalling code examples via Eclipse

To uninstall the code examples via Eclipse follow the below steps:

  * Right click on the project which you want to uninstall and select Uninstall (Native).
  ![](media/image11.png)

---

### Next

|-|
| ![](media/continue-arrow.png) **Want to find out more about the technologies available to applications in the Apertis environment?** Read about [Apertis technologies](sdk-techs.md). |
| ![](media/continue-arrow.png) **Need a quick reference for the Apertis platform APIs?** View the [Apertis API reference pages](apis.md) |
| ![](media/continue-arrow.png) **Ready to use the Apertis Development Environment?** Continue on to building and debugging with [the ADE tool](appdev-ade.md) |
