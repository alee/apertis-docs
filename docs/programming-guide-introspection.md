---
short-description: "APIs for languages other than C."

authors:
    - name: Ekaterina Gerasimova
      email: kat@collabora.co.uk
      years: [2016]
    - name: Philip Withnall
      email: philip@tecnocode.co.uk
      years: [2015]

license: CC-BY-SAv4.0
...

# Introspection

Apertis libraries are written in C and therefore all public APIs are available for app developers to use in C. Additionally, APIs should also have introspection enabled and therefore be available for languages other than C, such as JavaScript and Python.

[GObject introspection](https://wiki.gnome.org/Projects/GObjectIntrospection) (abbreviated ‘GIR’) is the system which Apertis uses to extracts APIs from C code and produces binary type libraries which can be used by non-C language bindings, and other tools, to [introspect](http://en.wikipedia.org/wiki/Type_introspection) or [wrap](http://en.wikipedia.org/wiki/Language_binding) the original C libraries.

Introspection cannot be enabled for programs, since they expose no APIs. However, it is still recommended to [add introspection annotations to documentation comments](#Introspection-annotations) in program code, as they clarify the documentation.

## Introspection annotations

Each comment should have appropriate [GObject introspection annotations](https://wiki.gnome.org/Projects/GObjectIntrospection/Annotations). Introspection annotations add information (functions, function parameters, function return values, structures, GObject properties, GObject signals) which is otherwise not present in a clear and consistent way.

In comments, annotations should be preferred over human-readable equivalents. For example, when documenting a function parameter which may be `NULL`, use the `(nullable)` annotation rather than some text:
```
/**
 * my_function:
 * @parameter: (nullable): some parameter which affects something
 *
 * Body of the function documentation.
 */
```

Instead of:
```
/**
 * my_bad_function:
 * @parameter: some parameter which affects something, or %NULL to ignore
 *
 * Bad body of the function documentation.
 */
```
