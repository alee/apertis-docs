---
short-description: "Frequently Asked Questions"

authors:
    - name: Micah Fedke
      email: micah.fedke@collabora.co.uk
      years: [2016]

license: CC-BY-SAv4.0
...

# FAQ

#### Q: What is Apertis?  
A: Apertis is a [FOSS](https://en.wikipedia.org/wiki/Free_and_open-source_software) ([Free](http://en.wikipedia.org/wiki/Free_software) and [open source](http://en.wikipedia.org/wiki/Open-source_software)) GNU/Linux-based platform for infotainment in automotive vehicles. It is a distribution derived from Debian/Ubuntu and geared towards the creation of product-specific images for ARM and Intel x86 systems.  

  Apertis comes with a range of built-in features, which can be expanded upon with custom applications that are available through an App Store.  

  It is not intended to support mission-critical features in vehicles (such as steering, brakes, engine control, ''etc''.), rather provide the information (for example, web navigation, weather information, geolocation, contacts, ''etc''.) and entertainment (for example, radio, music, videos, ''etc''.) features, in an app-centric fashion. It is updated regularly to keep track of features and security fixes.  

  The [Apertis wiki](https://wiki.apertis.org/) is the primary source of information about Apertis.  

#### Q: Where can I get help with developing applications for Apertis?  
A: The App Developer Portal you are reading right now is the primary source of documentation for Apertis application development.  You can also join the [mailing list for the Apertis project](https://wiki.apertis.org/Community) and ask your question there.  Your question may also have already been asked and/or answered in an [Apertis Phabricator](https://phabricator.apertis.org/search/query/advanced/) task.  

#### Q: Will my application work on all Apertis devices?
A: Apertis is a building block for product specific images targeting different devices with very different features: Apertis provides standard interfaces to help application writers to target many vendors and devices with little effort.  

#### Q: Can I write an Apertis application using Web technologies?
A: The Apertis Web runtime will be shipped as a technology preview in the upcoming Apertis 16.12 release.  

#### Q: How do I file a bug?
A: Apertis bugs can be filed in the [Apertis Phabricator](https://phabricator.apertis.org/maniphest/task/edit/form/8/).  

#### Q: How can I request to have a package added to Apertis?
A: Open a new task in the [Apertis Phabricator](https://phabricator.apertis.org/maniphest/task/edit/form/3/).  

#### Q: How do I request an API enhancement?
A: Open a new task in the [Apertis Phabricator](https://phabricator.apertis.org/maniphest/task/edit/form/3/).  

#### Q: How do I distribute my application for Apertis?
A: Distributing Apps for Apertis is covered in the [Distributing](app-distribution.md) section of this guide.
