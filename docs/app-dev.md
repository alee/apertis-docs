---
short-description: "A step by step guide for developing your first Apertis application"

authors:
    - name: Robert Bosch Car Multimedia GmbH
      years: [2015, 2016]
    - name: Micah Fedke
      email: micah.fedke@collabora.co.uk
      years: [2016]

license: CC-BY-SAv4.0
...

# Application development

Apertis is a next generation in-vehicle infotainment platform and SDK. It is an innovative operating system created for in-vehicle infotainment products. It is based on Linux Ubuntu.

## Required Knowledge

You should familiarise yourself with the [development environment in the SDK](sdk-usage.md) before continuing.

To understand and develop Apertis applications, you need to be comfortable with the following topics, libraries and tools:

* C Programming

* GLib

* GIO

* GObject (Glib Object System)

* Clutter

* GStreamer

* Automake

The resources listed below are good references to get started on these topics:

* [GObject Wikipedia Page](http://en.wikipedia.org/wiki/GObject)

* [GObject tutorial](https://developer.gnome.org/gobject/stable/howto-gobject.html)

* [GObject Reference Manual](http://library.gnome.org/devel/gobject/stable)

* [GLib Reference Manual](https://developer.gnome.org/glib/stable/)

* [GIO Reference Manual](https://developer.gnome.org/gio/stable/)

* [Clutter Tutorial](https://wiki.gnome.org/Projects/Clutter/Tutorial)

* [Clutter Project Wiki](https://wiki.gnome.org/Projects/Clutter)

* [GNU Automake](http://www.gnu.org/software/automake/)

* [GObject generator](https://github.com/GNOME/turbine/blob/master)

When creating a new project with the Apertis Eclipse plugin, sample application projects are available in "/home/user/sample-applications" path which can be imported for further reference.

Having said that, it is essential to have a basic understanding of the GObject and Apertis framework to be able to develop applications. Please use the links to gain a basic understanding of the GObject framework and read the subsequent section for an understanding of the code flow in the HelloWorld example code.

### Next

|-|
| ![](media/continue-arrow.png) **Ready to build your first app?** Continue on to building a [Hello World app](appdev-hello-world.md). |
