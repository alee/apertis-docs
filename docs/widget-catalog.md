---
short-description: "Widget catalog"

authors:
    - name: Gustavo Noronha Silva
      email: gustavo.noronha@collabora.co.uk
      years: [2016]

license: CC-BY-SAv4.0
...

# Widget catalog

The Apertis UI framework provides several UI components that can be leveraged
to build UIs for apps. Here is an overview of available widgets.

# Lightwood

The main component responsible for providing the UI building blocks is
**Lightwood**. Lightwood provides API interfaces and base implementation for
various widgets that can be reused to build UI components for variants
such as **Mildenhall**.

Lightwood uses Clutter as its base, so widgets are Clutter actors and the
Clutter APIs can be used with them.

## Interfaces

### Expandable <!-- lightwood-roller-liblightwood-expandable.html -->

An interface to be implemented by widgets that provide two modes of
operation, namely expanded and not expanded. It is mainly used inside
Lightwood for the `FixedRoller`, which supports expanding items when
they are selected.

## Abstract widgets

These are base classes which can be leveraged by variants for their concrete
implementations. They cannot be instantiated directly, only through a
subclass. Some concrete subclasses are provided by Lightwood, some are left
completely to the variants to define.

### Container <!-- lightwood-roller-liblightwood-container.html -->

A simple container widget. Doesn't do anything itself, only used as a bin to
contain other widgets. This can be thought of as the base for other widgets.

### Button <!-- lightwood-button-liblightwood-button.html -->

A button supporting several gestures. Presses and releases are joined by long
presses and also swipes. There is also support for variants to configure
tooltips. Those are not provided on the base widget.

### PopupBase <!-- lightwood-popup-liblightwood-popupbase.html -->

A base class for variants to use when implementing their popups. Contains
a property to indicate an audio file to be played when the popup appears.

### ProgressBase <!-- lightwood-progressbar-liblightwood-progressbase.html -->

Provides a media playback state bar. Visualization for media duration,
current time, buffering, as well as action buttons for play/pause and seeking.

### MapBase <!-- lightwood-map-liblightwood-mapbase.html -->

Provides a way of showing and interacting with maps. It is able to render
maps from OpenStreetMap and supports zooming and markers.

### Roller <!-- lightwood-roller-liblightwood-roller.html -->

A list of items resembling an infinite cylinder. It can be rotated, supports
kinetic behaviour and visual effects such as motion blur. Two concrete classes
are provided, with two different modes of operation: `FixedRoller` and
`VariableRoller`.

## Widgets

### MultilineEntry <!-- lightwood-textbox-liblightwood-multiline.html -->

Text entry with several lines. Displays and allows editing text with several
lines. Supports programmatic editing of the text, basic formatting of the
displayed text, as well as scrolling the lines of text.

### TextBox <!-- lightwood-textbox-liblightwood-textbox.html -->

Higher level text entry widget. Supports basic formatting of displayed text,
password entry, with entered characters converted to an asterisk. Although
it lacks the programmatic text editing capabilities of `MultilineEntry`, it
does allow editing several lines of text if its `multiline` property is turned
on.

### VariableRoller <!-- lightwood-roller-liblightwood-varroller.html -->

A specialization of the Roller which supports items with variable size.
By allowing each item to have a different size, more visual flexibility is
possible. Images of different sizes can be shown more naturally, for instance.
However, this makes the roller less efficient and can degrade performance
when there are many items.

### FixedRoller <!-- lightwood-roller-liblightwood-fixedroller.html -->

The roller can efficiently host many more items when their sizes are fixed,
since it doesn't need to ask all of its children about their sizes for layout.
This leads to some rigidity in the flow of the items, though. Images with
different aspect ratios will not look as natural, for instance. For that,
consider using VariableRoller.

## Expander <!-- lightwood-roller-liblightwood-expander.html -->

A container that can be clicked, tapped or dragged to show or hide its child.

### Flipper <!-- lightwood-flipper-liblightwood-flipper.html -->

An animated container for two actors. It places the actors on each other's
back and can switch between showing one or the other by performing a flip
animation ­— rotating both actors on their axes so that the one that had its
front toward the screen puts its back towards the screen and vice versa.

The way the rotation is done and the animation properties such as duration
and easing can be customized.

## Data

### Model <!-- lightwood-roller-liblightwood-model.html -->

A subclass of `ThornburyModel` that notifies when it is waiting for data.
In use cases where the model fetches data asynchronously from the disk
or from remote sources, it may happen that a request for more data does
not have an immediate response, the view needs to be notified. This is
an abstract class, so it cannot be instantiated directly. A concrete subclass
is necessary.

## Effects

### CylinderDeformEffect <!-- lightwood-roller-liblightwood-cylinderdeform.html -->

A ClutterEffect that can be applied to any ClutterActor. When applied to a
widget, the rendered content will be deformed so that it looks like a
cylinder. It is used on the `Roller` give it its cylindrical look.

### BlurEffect <!-- lightwood-roller-liblightwood-blureffect.html -->

A ClutterEffect that can be applied to any ClutterActor. When applied to a
widget, the rendered content will be blurred. It is used on the `Roller`
to provide a motion blur effect when scrolling fast.

### DeformPageTurn <!-- lightwood-roller-liblightwood-deformturn.html -->

A ClutterEffect that can be applied to any ClutterActor. When applied to a
widget, the rendered content will be deformed like a page that is being
turned.

### GlowShader <!-- lightwood-roller-liblightwood-glowshader.html -->

A ClutterEffect that can be applied to any ClutterActor. When applied to a
widget, the rendered content will have a glow around it. The `Roller` uses
this to highlight selected items, for instance.

# Mildenhall

The **Lightwood** library is a base upon which variants are built. The
reference variant implementation is **Mildenhall**.

## Widgets

### BottomBar <!-- MILDENHALL-widgets-BottomBar.html -->

A horizontal bar with 3 buttons and a text label.

### ButtonDrawer <!-- MildenhallButtonDrawer.html -->

A button which is also a drawer. It is built on `LightwoodButton`, and
thus supports all its features. The button can optionally have multiple
states other than normal and pressed, and swiping right and left can
change those states.

### RadioButton <!-- MildenhallRadioButton.html -->

A button which can be marked to select among exclusive alternatives. Several
radio buttons are placed on a single group and selecting one of them deselects
the others. It is built on `LightwoodButton`.

### ToggleButton <!-- MildenhallToggleButton.html -->

A button which has two states. Clicking or tapping switches between those
states.

<!-- There is also a MildenhallButtonSpeller, which is used for the
     virtual keyboard keys -->

### DrawerBase

A button which opens a drop-down or pull-up menu with more buttons. Beside
the buttons a tooltip is shown for a time describing it.

<!-- There are 3 subclasses to this: Context, Navi, Views. They are just
     for setting different hardcoded positions to the actor. -->

### LbsBottomBar <!-- MILDENHALL-widgets-LbsBottomBar.html -->

A horizontal bar with 3 labels arranged such that one is to the left,
one at the center, one to the right.

### LoadingBar <!-- MildenhallLoadingBar.html -->

A progress bar that can be used to show the status of tasks.

### MapWidget <!-- MapWidget.html -->

Shows a map and allows interaction with it. Zooming and markers are supported.

### MetaInfoFooter <!-- MILDENHALL-widgets-MetaInfoFooter.html -->

A horizontal bar providing an icon, a label and optional 5-star rating. It
can also display a decorative top bar.

### MetaInfoHeader <!-- MILDENHALL-widgets-MetaInfoHeader.html -->

A horizontal bar providing an icon, and a label. It can also display a
decorative bottom bar.

### Overlay <!-- MildenhallOverlay.html -->

An overlay that can place an image on top of something else.

### MediaOverlay <!-- MildenhallMediaOverlay.html -->

An overlay that can be placed on top of video. It includes a play/pause button
and text.

### ProgressBar <!-- MildenhallProgressBar.html -->

A media playback state widget, built on `LightwoodProgressBase`.
Visualization for media duration, current time, buffering, as well as action
buttons for play/pause and seeking.

### RatingBottomBar <!-- MildenhallRatingBottomBar.html -->

A horizontal bar with a 5-start rating or text label at the top-left and
2 text labels, one at the bottom left, one to the right.

### RollerContainer <!-- MILDENHALL-widgets-RollerContainer.html -->

The generic implementaton for the `LightwoodRoller`. It is actually not
a subclass, but rather a container that can host either a `FixedRoller` or
a `VariableRoller`.

### CabinetRoller <!-- MildenhallCabinetRoller.html -->

Allows navigation of hierarchical content, such as folders. It shows up to
two `Rollers` side by side. Selecting an item will show the contents of that
item on the right-side roller.

### InfoRoller <!-- MildenhallInfoRoller.html -->

A mix of a drawer and either a `FixedRoller` or `VariableRoller` from
Lightwood. The drawer can be opened to show the roller.

### PullupRoller <!-- MildenhallPullupRoller.html -->

A widget that mixes a drawer, a `MetaInfoFooter` from Mildehall and either
a `FixedRoller` or a `VariableRoller` from Lightwood. The drawer can
be opened to show the roller. It differs from `InfoRoller` in that it uses
a `MetaInfoFooter` to provide the base of the drawer.

### scroller <!-- MILDENHALL-widgets-mildenhall-scroller.html -->

A viewport that allows scrolling over content that overflows its size.
Supports finger scrolling with kinetic behaviour.

### SelectionPopup <!-- MildenhallSelectionPopup.html -->

A popup dialog that provides the user with a message and several buttons
that can be used to choose how to respond to the message.

### Speller <!-- MildenhallSpeller.html -->

A virtual keyboard. It works by using its own text entry widgets rather than
by sending key events immediately to the application. There are signals to
be notified when the user has pressed a key and when text is committed.

<!-- I don't think I understand the purpose of SpellerKeypad. How is it
     different from the reguar Speller? -->

### TextBox <!-- MildenhallTextBoxEntry.html -->

A text entry widget implementing `LightwoodTextBox`. Supports basic
formatting of displayed text, password entry, with entered characters converted
to an asterisk. Allow editing several lines of text if its `multiline` property
is turned on.

### WebViewWidget <!-- MildenhallWebViewWidget.html -->

A widget providing a web engine. It can be used to render local and remote web
pages.

<!-- MildenhallWebViewWidgetAlertHandler, MildenhallWebViewRenderTheme,
     and MildenhallWebViewSoupAuthDialog are variant-specific UI for the
     browser engine and would not be used in general. -->

### WidgetContainer <!-- MildenhallWidgetContainer.html -->

A container that can be used to lay out other widgets. It supports animating
widgets when moving them inside its viewport.

### PopupInfo <!-- PopupInfo.html -->

Implementation of `LightwoodPopupBase`. It is able to show text and 3 buttons.

## Roller item widgets

The `Roller` widgets can be populated by several types of items. These
are some of the items provided by Mildenhall. The developer decides which
type of item to use and the `Roller` will create them as needed based
on the model.

<!-- there is a CabinetItem, which I am seeing as an implementation detail
     of the cabinet roller -->

### DetailRoller <!-- MILDENHALL-widgets-DetailRoller.html -->

A `Roller` item for showing several icons and several labels of text.

<!-- DiskUsageItem is a roller item that provides an horizontal bar showing
     percentage, along with icon and text. -->

### IconLabelIconItem <!-- IconLabelIconItem.html -->

A `Roller` item to display a label and two icons, one at each side of the
label. It also supports showing progress information using a
`MildenhallLoadingBar`.

### IconLabelRadioButtonItem <!-- IconLabelRadioButtonItem.html -->

A `Roller` item to display an icon, a label and a radio button.

### IconTextVariable <!-- IconTextVariableItem.html -->

A `Roller` item for showing an image, a label and optionally a 5-star
rating.

### ListItem <!-- ListItem.html -->

A `Roller` item with an icon and four labels.

### LbsListRollerItem <!-- LbsListRollerItem.html -->

A `Roller` item with 3 labels placed such that one is at the top, two at
the left and right sides of the bottom, with an icon in the middle.

### MessageRollerItem <!-- MILDENHALL-widgets-MessageRollerItem.html -->

A widget capable of showing an email message, fit for using on `Roller`
widgets.

<!-- RollerItemBT and RollerItemConnectivity seem to be specific to  Bluetooth
     and Internet connection settings -->

### SampleDetailItem <!-- SampleDetailItem.html -->

A `Roller` item showing a short text and a long text.

<!-- SampleItem looks too specific, there is an app-type property even -->

### SampleThumbnailItem <!-- SampleThumbnailItem.html -->

Shows images as `Roller` items.

### SampleVariableItem <!-- SampleVariableItem.html -->

Item showing icon and label with variable height, for use with
`VariableRoller`.

### SBrowserDetail <!-- MildenhallSBrowserDetail.html -->

Shows a video or audio inside a `Roller`.

<!-- I'm considering SelectionPopupItem an implementation detail of
     SelectionPopup -->

### SingleItem <!-- SingleItem.html -->

`Roller` item with 3 text labels and optional footer

### SortRoller <!-- MILDENHALL-widgets-SortRoller.html -->

A `Roller` item with data that can be sorted by its parent either
alphabetically or numerically.

## Speller entry widgets

The virtual keyboard provided by Mildenhall supports several types of entries
where text can be typed. The entry type can be specified as a property of the
`Speller` object.

### SpellerDefaultEntry <!-- MildenhallSpellerDefaultEntry.html -->

The default entry type used by the `Speller`. It provides autocomplete
and history features.

### SpellerFourToggleEntry <!-- MildenhallSpellerFourToggleEntry.html -->

An entry that includes a text entry and four `MildenhallToggleButton`
instances.

### SpellerMultiLineEntry <!-- MildenhallSpellerMultiLineEntry.html -->

Text entry where several lines of text can be edited at once.

### SpellerMultipleEntry <!-- MildenhallSpellerMultipleEntry.html -->

Several entries that can be edited on the same virtual keyboard.

## ThumbnailItem <!-- MILDENHALL-widgets-ThumbnailItem.html -->

Item for the `Roller` hosting a `MediaOverlay` widget.

## VideoListRollerItem <!-- MILDENHALL-widgets-VideoListRollerItem.html -->

Item with 3 text labels and an icon for the `Roller`.

### VideoThumbRoller <!-- MILDENHALL-widgets-VideoThumbRoller.html -->

Item to show an image and optionally a video player on a `Roller`.
