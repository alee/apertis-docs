---
short-description: "Apertis platform APIs"

authors:
    - name: Micah Fedke
      email: micah.fedke@collabora.co.uk
      years: [2016]

license: CC-BY-SAv4.0
...

# Apertis APIs
# Platform libraries used by applications

## Traprain
| Purpose | API Documentation | Git repository | Description |
|---------|-------------------|----------------|-------------|
| Navigation and routing libraries | [Traprain API](https://docs.apertis.org/traprain/traprain-0/) | <https://gitlab.apertis.org/appfw/traprain> | Traprain is a set of libraries allowing navigation services, such as a car GPS routing application, to share navigation and routing information to third party applications.|

## libgrassmoor
| Purpose | API Documentation | Git repository | Description |
|---------|-------------------|----------------|-------------|
| Media information and playback library | [libgrassmoor API](https://docs.apertis.org/libgrassmoor/grassmoor/) | <https://gitlab.apertis.org/hmi/libgrassmoor> | Libgrassmoor is a library responsible for providing media info and media playback functionalities. |

## liblightwood
| Purpose | API Documentation | Git repository | Description |
|---------|-------------------|----------------|-------------|
| Widget library | [liblightwood API](https://docs.apertis.org/liblightwood/lightwood/) | <https://gitlab.apertis.org/hmi/liblightwood> | LibLightwood is a widget library.  It has all the basic widgets like buttons, lists, rollers, webviews, widgets, multilines and textboxes, and can be extended to add functionality to these base widgets. See the [widget catalog](widget-catalog.md#lightwood).|

## Mildenhall
| Purpose | API Documentation | Git repository | Description |
|---------|-------------------|----------------|-------------|
| User interface widget library | [Mildenhall API](https://docs.apertis.org/mildenhall/MILDENHALL-widgets/) | <https://gitlab.apertis.org/hmi/mildenhall> | Mildenhall is a platform library providing the reference UI widgets, used by graphical applications.  See the [widget catalog](widget-catalog.md#mildenhall).|

## libseaton
| Purpose | API Documentation | Git repository | Description |
|---------|-------------------|----------------|-------------|
| Persistent data management library | [libseaton API](https://docs.apertis.org/libseaton/libseaton/) | <https://gitlab.apertis.org/hmi/libseaton> | LibSeaton provides interfaces to store persistent data. |

## libthornbury
| Purpose | API Documentation | Git repository | Description |
|---------|-------------------|----------------|-------------|
| UI utility library | [libthornbury API](https://docs.apertis.org/libthornbury/libthornbury/) | <https://gitlab.apertis.org/hmi/libthornbury> | LibThornbury provides helper functions for customized JSON parsing. |


# Platform user-services used by applications

## Barkway
| Purpose | API Documentation | Git repository | Description |
|---------|-------------------|----------------|-------------|
| Global popup management framework | [Barkway API](https://docs.apertis.org/barkway/barkway/) | <https://gitlab.apertis.org/appfw/barkway> | Barkway is a platform component, consisting of a user service and libraries that interface with it. |

## Canterbury
| Purpose | API Documentation | Git repository | Description |
|---------|-------------------|----------------|-------------|
| Application management and process control service | [Canterbury API](https://docs.apertis.org/canterbury/canterbury/) | <https://gitlab.apertis.org/appfw/canterbury> | Canterbury is a platform component, consisting of a user service and libraries that interface with it. |

## Didcot
| Purpose | API Documentation | Git repository | Description |
|---------|-------------------|----------------|-------------|
| Data sharing and file opening service | [Didcot API](https://docs.apertis.org/didcot/didcot/) | <https://gitlab.apertis.org/appfw/didcot> | Didcot is a platform component, consisting of a user service and libraries that interface with it. It mediates file opening by media type (content type, MIME type), URL opening by URL scheme, and other data sharing between user applications. |

## Newport
| Purpose | API Documentation | Git repository | Description |
|---------|-------------------|----------------|-------------|
| Download manager | [Newport API](https://docs.apertis.org/newport/newport/) | <https://gitlab.apertis.org/appfw/newport> | Newport is a platform component containing a user service, and a library for communication with the service. It manages large downloads for applications and other services. |

## Prestwood
| Purpose | API Documentation | Git repository | Description |
|---------|-------------------|----------------|-------------|
| Disk mounting service | [Prestwood API](https://docs.apertis.org/prestwood/prestwood/) | <https://gitlab.apertis.org/hmi/prestwood> | Prestwood is a platform component containing a user service and a library for communication with the service. It mounts removable media. |

## Tinwell
| Purpose | API Documentation | Git repository | Description |
|---------|-------------------|----------------|-------------|
| Media playback service | [Tinwell API](https://docs.apertis.org/tinwell/tinwell/) | <https://gitlab.apertis.org/hmi/tinwell> | Tinwell is a platform component containing a user service and a library for communication with the service. It plays media. |

## Ribchester
| Purpose | API Documentation | Git repository | Description |
|---------|-------------------|----------------|-------------|
| Application installer/mounting service | [Ribchester API](https://docs.apertis.org/ribchester/ribchester/) | <https://gitlab.apertis.org/appfw/ribchester> | Ribchester is a service responsible for managing the application and services mount point and preparing the partitions based on the framework. |

## Rhosydd
| Purpose | API Documentation | Git repository | Description |
|---------|-------------------|----------------|-------------|
| Handling access to sensors and actuators | [Rhosydd API](https://docs.apertis.org/rhosydd/librhosydd-0/) | <https://gitlab.apertis.org/appfw/rhosydd> | Rhosydd is a system service for handling access to sensors and actuators from applications. It provides a vendor-specific hardware API to connect to the services which control the hardware, and a SDK API for applications to use to access it. |
| Sensor backend utilities                 | [Croesor API](https://docs.apertis.org/rhosydd/libcroesor-0/) | <https://gitlab.apertis.org/appfw/rhosydd> | Rhosydd is a system service for handling access to sensors and actuators from applications. It provides a vendor-specific hardware API to connect to the services which control the hardware, and a SDK API for applications to use to access it. |

# Upstream APIs
## Enabling APIs

These are the libraries that make up the supported API surface made available for applications from the store to use. Effort may be made to ease or remove transition issues when ABI or API breakages happen.

### Content Rendering

| Component | Packages | Category |
|-----------|----------|----------|
| [WebKitGTK+](https://webkitgtk.org/) | libwebkit2gtk-4.0 | Web engine |
| [Poppler](https://poppler.freedesktop.org/) | libpoppler-glib8, libpoppler28 | PDF rendering |

### UI

| Component | Packages | Category |
|-----------|----------|----------|
| [Cairo](http://cairographics.org/) | libcairo2 | Drawing library |
| [GTK+ 3](https://developer.gnome.org/gtk3/stable/) | libgtk-3-0 | UI toolkit |
| [Qt](https://www.qt.io/) | libQt5Gui | UI toolkit |

### Services

| Component | Packages | Category |
|-----------|----------|----------|
| [Canberra](http://0pointer.de/lennart/projects/libcanberra/gtkdoc/) | libcanberra0 | High-level sounds |
| [PolicyKit](https://www.freedesktop.org/software/polkit/docs/latest/polkit.8.html) | policykit-1  libpolkit-agent-1-0 libpolkit-backend-1-0 libpolkit-gobject-1-0 | System |
| [Folks](http://telepathy.freedesktop.org/doc/folks/c/) | libfolks-eds25, libfolks-telepathy25, libfolks25 | Contacts & calendar |
| [Grilo](http://developer.gnome.org/grilo/0.2/) | libgrilo-0.2-1 | Media indexing |
| [Tracker-extract](https://developer.gnome.org/libtracker-extract/0.16/) [Tracker-miner](https://developer.gnome.org/libtracker-miner/0.16/) [Tracker-sparql](https://developer.gnome.org/libtracker-sparql/0.16/) | libtracker-extract-0.16-0, libtracker-miner-0.16-0, libtracker-sparql-0.16-0 | Media indexing |
| [libsecret](https://developer.gnome.org/libsecret/0.16/) | libsecret-1-0 | Secrets management |
| [Telepathy](http://telepathy.freedesktop.org/doc/book/index.html) | libtelepathy-glib0 libtelepathy-farstream3 | Communication |
| [ofono](https://01.org/ofono) | onfono | oFono is a stack for mobile telephony devices on Linux |

## OS APIs

These are more fundamental APIs, usually used by the Enabling APIs to provide higher-level functionality. Platform upgrades may require adapting applications to new APIs and/or ABIs for these.

### Infrastructure

| Component | Packages | Category |
|-----------|----------|----------|
| [eglibc](http://www.gnu.org/software/libc/manual/html_node/index.html) | libc6 | OS interfaces |
| [GLib](https://developer.gnome.org/glib/stable/) | libglib2.0-0 | Basic programming framework |
| [systemd](https://www.freedesktop.org/wiki/Software/systemd/) | systemd | Service manager |
| [OSTree](http://ostree.readthedocs.io/) | libostree | Atomic software updates |

### Rendering building blocks

| Component | Packages | Category |
|-----------|----------|----------|
| [pixman](http://www.pixman.org/) | libpixman-1-0 | Low-level graphics |
| [Pango](https://developer.gnome.org/pango/stable/) | libpango1.0-0 | High-level font rendering |
| [Mesa](http://mesa3d.org/sourcedocs.html) | libegl1-mesa libgbm1 libgl1-mesa-dri libglapi-mesa libgles2-mesa | Low-level graphics |
| [harfbuzz](https://www.freedesktop.org/wiki/Software/HarfBuzz/) | libharfbuzz0 | Low-level font rendering |
| [freetype](https://www.freetype.org/freetype2/docs/documentation.html) | libfreetype6 | Low-level font rendering |

### Functionality

| Component | Packages | Category |
|-----------|----------|----------|
| [gdk-pixbuf](https://developer.gnome.org/gdk-pixbuf/2.36/) | libgdk-pixbuf2.0-0 | Image manipulation |
| [GMime](https://developer.gnome.org/gmime/stable/) | libgmime-2.6-0 | File format support |
| [GStreamer](https://gstreamer.freedesktop.org/documentation/) | libgstreamer1.0-0 gstreamer1.0-plugins-base libgstreamer-plugins-base1.0-0 gstreamer1.0-plugins-good libgstreamer-plugins-good1.0-0 | Low-level multimedia |
| [Farstream](https://www.freedesktop.org/wiki/Software/Farstream/#documentation) | libfarstream-0.2-2 | Communication |
| [libxml2](http://www.xmlsoft.org/docs.html) | libxml2 | File format support |
| [libxslt](http://xmlsoft.org/libxslt/docs.html) | libxslt1.1 | File format support |
| [SQLite](https://sqlite.org/docs.html) | libsqlite3-0 | Data storage |
| [JSON GLib](https://developer.gnome.org/json-glib/1.2/) | libjson-glib-1.0-0 | File format support |
| [Soup](https://developer.gnome.org/libsoup/stable/) | libsoup-gnome2.4-1, libsoup2.4-1 | Network protocol |
| [LLVM](http://llvm.org/docs/) | libllvm3.2 | Compiler technology |
| [Bluez](http://www.bluez.org/) | bluez | Bluetooth technology |


Note that Apertis deploys only license verified Gstreamer plugins. Basically, each Gstreamer plugin is developing under [https://www.gnu.org/licenses/lgpl.html LGPL], but only [http://cgit.freedesktop.org/gstreamer/gstreamer gstreamer core], [http://cgit.freedesktop.org/gstreamer/gst-plugins-base base] and [http://cgit.freedesktop.org/gstreamer/gst-plugins-good good] provide license and patent-free plugins. In case of [http://cgit.freedesktop.org/gstreamer/gst-plugins-ugly gst-plugins-ugly] and [http://cgit.freedesktop.org/gstreamer/gst-plugins-bad gst-plugins-bad], they are not immune from the license and patent issues. gst-plugins-bad usually provides a space to review API stablity as well as license problem. In addition, the plugins which have potential issues with patents, or license fee should be located in gst-plugins-ugly.For more information, please refer to http://gstreamer.freedesktop.org/licensing.html  

## How to proceed if a required package or library is not available
If your application depends on a package that is not including in Apertis, please open a task in the [Apertis Phabricator](https://phabricator.apertis.org/) requesting the addition of the new package.  Be sure to include the rationale behind your request.
