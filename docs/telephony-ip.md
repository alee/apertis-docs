---
short-description: "Online communication services"

authors:
    - name: Robert Bosch Car Multimedia GmbH
      years: [2015, 2016]

license: CC-BY-SAv4.0
...

# VoIP, IM and groupware

### Provides:

- IP telephony

- Chat, *etc*.

### Enabling components:

- Telepathy-GLib
  (http://telepathy.freedesktop.org/doc/telepathy-glib/)

- Telepathy Developer's Manual
  (http://telepathy.freedesktop.org/doc/book/index.html)

- libGData (https://developer.gnome.org/gdata/unstable/)

- SyncEvolution D-Bus (http://api.syncevolution.org/)
